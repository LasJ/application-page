import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeComponent } from './welcome/welcome.component';
import { ViewsModule } from '../views/views.module';


@NgModule({
  declarations: [
    WelcomeComponent
  ],
  imports: [
    CommonModule,
    ViewsModule
  ],
  exports: [
    WelcomeComponent
  ]
})
export class WelcomeModule { }
