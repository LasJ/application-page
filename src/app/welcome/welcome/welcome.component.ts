import { Component, OnInit } from '@angular/core';
import { ScrollHandler, ScrollListener } from 'src/app/helper/scroll-handler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit, ScrollListener {

  scrollHandler: ScrollHandler = ScrollHandler.getInstance(this)

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.scrollHandler.kill()
  }

  ngAfterViewInit(): void {
    let contentView = document.getElementById("content-welcome")
    if (contentView)
      this.scrollHandler.init(contentView)
  }

  onOverscrollBottomMax(): void {
    this.router.navigateByUrl("/story/0")
  }

  onOverscrollTopMax(): void {
    // Do nothing
  }

}
