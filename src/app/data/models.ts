export interface StoryItem {
    name: string | undefined,
    text: string | undefined,
    subText: string | undefined,
    time: string | undefined,
    imageUrl: string | undefined,
    images: string[] | undefined,
    selected?: boolean
}


export interface SkillCategory {
    name: string | undefined,
    items: SkillItem[] | undefined,
    selected?: boolean
}

export interface SkillItem {
    name: string | undefined,
    text: string | undefined,
    images: string[] | undefined,
    videos: string[] | undefined,
    selected?: boolean
}