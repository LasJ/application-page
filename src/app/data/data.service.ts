import { Injectable } from '@angular/core';
import { SkillCategory, StoryItem } from './models';
import { Observable, of } from 'rxjs';

const STORY_ITEMS: StoryItem[] = [
  { name: 'Welcome', text: "Are you looking for a <span class='highlight'>developer</span>? My comfort zones are Android and KMM. I'm also creating products on iOS, Web (Angular), Garmin (MonkeyC) and backend (JS/TS and MongoDB). Side skills are Figma, Photoshop, Illustrator and blockchain theory. I have experience in founding and also enjoyed leading a team from an idea to a running product.", subText: "Curious? Scroll on.", time: 'today', imageUrl: './assets/images/img_las_small.png', images:[]},
  { name: 'Home Connect', text: "I'm currently working for <a href='https://uxma.com/' target='_blank'>UXMA</a>, a software- and design agency based in Kiel, Germany. The project I'm on since day one is called <a href='https://www.home-connect.com/' target='_blank'> Home Connect</a> - an app for controlling home appliances of various brands like Bosch, Siemens, Neff or Gaggenau. My main task is the development of the Android frontend, including the re-implementation of the apps main control screen, app brandification and building a module for a new appliance type. I also put my toe in the water regarding microservice development. I like the project but can't stay, since my family and me want to change countries.", subText: "#CleanArchitecture #Modularization #Compose #DI #Testing #Scrum",  time: "mar '22 - today", imageUrl: './assets/icons/homeconnect.png', images:[]},
  { name: 'H2Coach', text: "<a href='https://play.google.com/store/apps/details?id=com.fjordev.h2coach' target='_blank'>H2Coach</a> is my <span class='highlight'>app for swim training</span>. We developed our own didactical System, founded a company (GmbH), tested and published the app, reached 100 subscriptions but got slowed down by Covid, which is why I switched to a solid job.", subText: "#Android #Garmin #NodeJS #ParseServer #Algorithm #Figma #Photoshop #PremierePro #Illustrator #Founding #TeamOrga #Prototyping", time: 'since 2018', imageUrl: './assets/icons/h2coach.png', images:['./assets/images/h2coach_1.png','./assets/images/h2coach_2.png','./assets/images/h2coach_3.png'] },
  { name: 'Pool Log', text: "Pool Log is a <span class='highlight'>web tool for water meter documentation</span>. I developed it for our local swimming hall <a href='https://campusbad-fl.de/' target='_blank'>Campusbad Flensburg</a>. It is in daily use and based on Angular.", subText: "#Angular #ParseServer #OfflineMode #BackgroundAnalysis #ExcelExport", time: '2020', imageUrl: './assets/icons/poollog.png', images:['./assets/images/poollog_1.png'] },
  /*{ name: 'Relationship Goal', text: "<a href='https://bringliebemit.de' target='_blank'>Bring Liebe Mit</a> is a simple <span class='highlight'>onlineshop</span> my fiance and I run. Target for this year: Version 2.0 with customization preview based on Angular.", subText: "Next up: educational background", time: 'since 2021', imageUrl: './assets/icons/blm.png', images:[] },*/
  { name: 'Android', text: "I wanted to develop my own apps, so I started <span class='highlight'>Android</span> with <a href='https://www.udacity.com/course/developing-android-apps-with-kotlin--ud9012' target='_blank'>this</a> <span class='highlight'>course</span> alongside my study and applied the knowledge with the prototype of H2Coach.", subText: "Meanwhile...", time: '2018', imageUrl: './assets/icons/android.png', images:[] },
  { name: 'HSFL', text: "To make sure I got the best practices right I joined <span class='highlight'>applied informatics</span> at the HS Flensburg, picked the <a href='./assets/docs/hsfl_modules.pdf' target='_blank'>modules</a> I needed for H2Coach, condensed them to 3 semesters and then started the company.", subText: "#OOP #StructuredCoding #Databases #InterfaceDesign #WebDev #WebTech #WebDesign #Java #JS #Go", time: "sep '17 - feb '19", imageUrl: './assets/icons/hsfl.png', images:[] },
  { name: 'Chainstep', text: "To finance my study I worked for Chainstep, a Hamburg based <span class='highlight'>blockchain company</span> as a dev with the goal to help them get an overview over different chains. They found me via my master thesis.", subText: "#Vue #Docker #BashScript #Ethereum #IPFS #Iota", time: "feb '18 - feb '19", imageUrl: './assets/icons/chainstep.png', images:[] },
  { name: 'TU Hamburg', text: "I did my <span class='highlight'>logistics master at TUHH</span> and decided to focus on coding. That's why I wrote my <a href='./assets/docs/tuhh_thesis.pdf' target='_blank'>thesis</a> about blockchain where I built a private Ethereum network, got into smart contracts and somehow managed a 1,0.", subText: "Next up: Skill summary", time: "oct '15 - sep '17", imageUrl: './assets/icons/tuhh.png', images:[] },
]

const SKILL_ITEMS: SkillCategory[] = [
  {name: 'Coding', items: [
    {name:'Android', text:"Android is my favorite playground. I like to use use clean architecture, modularization and MVI. In private projects I implemented payment processing, Garmin watch communication, video streaming, PDF exports and offline functionality to name a few.", images:['./assets/images/android_1.png','./assets/images/android_2.png','./assets/images/android_3.png','./assets/images/android_4.png','./assets/images/android_5.png','./assets/images/android_6.png','./assets/images/android_7.png'], videos:[]},
    {name:'KMM', text:"I am currently working on a new private project which I write in <a href='https://kotlinlang.org/docs/multiplatform.html' target='_blank'>Kotlin Multiplatform</a>. Also the next H2Coach version will be refactored based on this. I also like to use <a href='https://www.jetbrains.com/de-de/lp/compose-multiplatform/' target='_blank'>Compose Multiplatform</a> - mixed with native UI if necessary.", images:[], videos:[]},
    {name:'Angular', text:"I like to develop web projects in Angular. Examples of my work are Pool Log and the page you're looking at. Some things I did are an offline mode, data entry sessions, role management and Excel analysis export. The code for this site can be found <a href='https://bitbucket.org/LasJ/application-page/' target='_blank'>here</a>.", images:['./assets/images/angular_1.png', './assets/images/angular_2.png', './assets/images/angular_3.png'], videos:[]},
    {name:'Garmin', text:"I know how to write Garmin apps in Monkey C. The H2Coach watch app accepts workouts from iOS / Android, saves the execution and feedback during the training and sends it to the server via the phone when reconnected. The watch app can be found <a href='https://apps.garmin.com/en-US/apps/cef45416-f8ec-4189-befd-6d84fcd6ab9b' target='_blank'>here</a>.", images:['./assets/images/garmin_1.png', './assets/images/garmin_2.png', './assets/images/garmin_3.png', './assets/images/garmin_4.png', './assets/images/garmin_5.png'], videos:[]},
    {name:'Backend', text:"I've built the backends for all private projects so far with <a href='https://www.back4app.com/' target='_blank'>Back4App</a>, which uses Parse Server and MongoDB. I avoid the offered frontend sdk and rather request my own API written in NodeJS.", images:[], videos:[]}
  ]},
  {name: 'Media', items: [
    {name:'Figma', text:"Figma is my go to tool for designing apps and sites. Any private project I work on is first designed in it and I like to keep the design updated as the basis for the development process.", images:[], videos:[]},
    {name:'Illustrator', text:"I use Illustrator since about 7 years for pretty much every project or digital design task.", images:['./assets/images/illustrator_1.png', './assets/images/illustrator_2.png', './assets/images/illustrator_3.png'], videos:[]},
    {name:'Photoshop', text:"I like to use Photoshop for private and professional image editing since about 10 years. There's still a lot in this mighty tool that I don't know about but most tasks get done quickly.", images:['./assets/images/photoshop_1.png', './assets/images/photoshop_2.png', './assets/images/photoshop_3.png'], videos:[]},
    {name:'Premiere', text:"I edit videos (e.g. for H2Coach) with Adobe Premiere Pro and know a bit of After Effects.", images:[], videos:['./assets/videos/premiere_3.mp4']},
    {name:'Ads', text:"We ran Instagram and Facebook Ad campaigns for H2Coach which we were able to bring down to a CPI of 50 cent in Germany - during non covid times. An Etsy shop my wife runs is advertised for with click oriented campaigns.", images:['./assets/images/ads_1.png','./assets/images/ads_2.png','./assets/images/ads_3.png','./assets/images/ads_4.png'], videos:[]}
  ]},
  {name: 'Soft', items: [
    {name:'Languages', text:'I speak German natively, English daily, Spanish poorly and just started to learn Danish.', images:[], videos:[]},
    {name:'Prototyping', text:"Please excuse the long text, I have to stress this: The one thing I enjoy most about creating digital products is building something that is of value for many others. To ensure this I prototype close to the user.<br><br>H2Coach started with provisional videos on a smartphone that we showed to our testers during training. Followed by mockup tests, and a MVP app. We faced pivots, tested each new feature and did long term tests with groups to check the results of the app vs. classic group training.", images:[], videos:[]},
    {name:'Teamplay', text:"In peak times H2Coach was worked on by 4 People, so the coordination is not super complex. But I enjoy the process very much and also would like to think that it makes me understand how to be more effective as a team member.", images:[], videos:[]},
    {name:'Economics', text:"My study involved business administration, which served me well during the founding of our company. The founding also gave me a better understanding for accounting and legal issues. I enjoy the process of modeling business cases - preferably in combination with personas and quick testing.", images:[], videos:[]},
    {name:'Personality', text:"I like to break down my work and do one thing at a time. I'm hungry for new knowledge about coding, tech, business and management. I like clear and regular communication. I'm able to work independently.<br><br>My weakness is going into discussions without listening. It's gotten way better! I love going all the way from an idea to a working product. Being honest and reliable is important to me and I'm motivated - since I found what I love to do.", images:[], videos:[]}
  ]},
]

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getStoryItems(): Observable<StoryItem[]> {
    return of(STORY_ITEMS)
  }

  getSkillItems(): Observable<SkillCategory[]> {
    return of(SKILL_ITEMS)
  }
}
