import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { SkillCategory, SkillItem } from 'src/app/data/models';
import * as _ from 'lodash';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Input() skillCategories: SkillCategory[] = []
  @Input() currentSkillItems: SkillItem[] = []
  @Input()
  get currentIndex(): [number, number] { return this._currentIndex }
  set currentIndex(currentIndex: [number, number]) {
    this._currentIndex = currentIndex
    this.updateSkillItems()
    setTimeout(() => { this.updateUI() }, 0);
  }
  private _currentIndex: [number, number] = [0, 0]

  @Output() currentIndexChange: EventEmitter<[number, number]> = new EventEmitter()

  constructor() { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.setResizeListener()
    this.updateUI()
  }

  private setResizeListener(): void {
    window.addEventListener("resize", _.debounce(() => {
      this.updateUI()
    }, 100));
  }

  private updateSkillItems(): void {
    let newItems
    if (this.skillCategories.length > this.currentIndex[0])
      newItems = this.skillCategories[this.currentIndex[0]].items
    this.currentSkillItems = newItems != undefined ? newItems : []
  }

  public changeCurrentIndex(newIndex: [number, number]) {
    this.currentIndexChange.emit(newIndex)
  }

  private updateUI() {
    this.setBoxMargins(this.currentIndex[0], this.skillCategories.length, 'category')
    this.setBoxMargins(this.currentIndex[1], this.currentSkillItems.length, 'item')
  }

  private setBoxMargins(index: number, arraySize: number, middleId: string) {
    if (index < 0 || index >= arraySize)
      return
    let firstBox: HTMLElement | null = document.getElementById('box-' + middleId + 0)
    if (window.innerHeight > window.innerWidth) {
        if (firstBox)
          this.setContainerMargin(0, 1, middleId, firstBox)
    } else {
      if (firstBox)
        this.setContainerMargin(index, arraySize, middleId, firstBox)
      let xMargin = 10
      for (let i = 0; i < arraySize; i++) {
        let box: HTMLElement | null = document.getElementById('box-' + middleId + i)
        if (box) {
          let marginLeft = xMargin - Math.abs(index - i) * xMargin + 'px'
          box.animate([{marginLeft: box.style.marginLeft},{marginLeft: marginLeft}], {duration: 100})
          box.style.marginLeft = xMargin - Math.abs(index - i) * xMargin + 'px'
        }
      }
    }
  }

  private setContainerMargin(index: number, arraySize: number, middleId: string, box: HTMLElement): void {
    let container: HTMLElement | null = document.getElementById('container-' + middleId)
    if (container) {
      let boxHeight: number = box.getBoundingClientRect().height
      let boxesAbove: number = arraySize / 2 - index - 0.5
      let marginTop: string = boxHeight * boxesAbove * 2 + 'px'
      container.animate([{marginTop: container.style.marginTop},{marginTop: marginTop}], {duration: 100})
      container.style.marginTop = marginTop
    }
  }
}
