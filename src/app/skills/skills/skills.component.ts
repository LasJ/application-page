import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/data/data.service';
import { SkillCategory, SkillItem } from 'src/app/data/models';
import { ScrollHandler, ScrollListener } from 'src/app/helper/scroll-handler';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit, ScrollListener {

  _currentIndex: [number, number] = [0, 0]
  get currentIndex() {
    return this._currentIndex
  }
  set currentIndex(currentIndex: [number, number]) {
    this._currentIndex = currentIndex
    this.updateContent()
  }

  contentView: HTMLElement | null = null
  scrollHandler: ScrollHandler = ScrollHandler.getInstance(this)
  subRouteSubscription: Subscription | undefined
  skillCategories: SkillCategory[] = []
  currentSkillItems: SkillItem[] = []

  constructor(private dataService: DataService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getSkillItems()
    this.startSubRouteSubscription()
  }

  ngAfterViewInit(): void {
    this.contentView = document.getElementById("content-skills")
    if (this.contentView)
      this.scrollHandler.init(this.contentView)
    this.setNavScrollListener()
  }

  ngOnDestroy(): void {
    this.scrollHandler.kill()
    this.subRouteSubscription?.unsubscribe()
  }

  private startSubRouteSubscription(): void {
    this.subRouteSubscription = this.activatedRoute.firstChild?.params.subscribe(params => {
      this.currentIndex = [+params['id1'], +params['id2']]
    });
  }

  private setNavScrollListener() {
    let navView: HTMLElement | null = document.getElementById("nav-skills")
    if (!navView)
      return
    navView.addEventListener('wheel', (event) => {
      if (event.deltaY < 0)
        this.decreaseIndex(false)
      else if (event.deltaY > 0)
        this.increaseIndex(false)
    })
  }

  private getSkillItems(): void {
    this.dataService.getSkillItems().subscribe(
      items => {
        this.skillCategories = items
      }
    )
  }

  private updateContent() {
    if (this.currentIndex[0] >= this.skillCategories.length) {
      let itemLength: number | undefined = this.skillCategories[this.skillCategories.length - 1].items?.length
      this.router.navigateByUrl("/skills/" + (this.skillCategories.length - 1) + "/" + ((itemLength ? itemLength : 0) - 1), { replaceUrl: true })
    } else {
      this.router.navigateByUrl("/skills/" + this.currentIndex[0] + "/" + this.currentIndex[1], { replaceUrl: true })
    }
    this.updateSkillItems()
    this.resetContentScroll()
  }

  private updateSkillItems(): void {
    let newItems
    if (this.skillCategories.length > this.currentIndex[0])
      newItems = this.skillCategories[this.currentIndex[0]]?.items
    this.currentSkillItems = newItems != undefined ? newItems : []
  }

  private resetContentScroll() {
    setTimeout(() => { this.contentView?.scrollTo(0, 0) }, 1);
  }

  private decreaseIndex(exitPossible: boolean) {
    if (this.currentIndex[1] <= 0) {
      if (this.currentIndex[0] <= 0) {
        if (exitPossible)
          this.exit(true)
      } else {
        let itemLength: number | undefined = this.skillCategories[this.currentIndex[0] - 1].items?.length
        this.currentIndex = [this.currentIndex[0] - 1, itemLength ? itemLength - 1 : 0]
      }
    } else {
      this.currentIndex = [this.currentIndex[0], this.currentIndex[1] - 1]
    }
  }

  private increaseIndex(exitPossible: boolean) {
    let itemLength: number | undefined = this.skillCategories[this.currentIndex[0]].items?.length
    if (this.currentIndex[1] >= (itemLength ? itemLength : 0) - 1) {
      if (this.currentIndex[0] >= this.skillCategories.length - 1) {
        if (exitPossible)
          this.exit(false)
      } else {
        this.currentIndex = [this.currentIndex[0] + 1, 0]
      }
    } else {
      this.currentIndex = [this.currentIndex[0], this.currentIndex[1] + 1]
    }
  }

  private exit(prev: boolean) {
    if (prev)
      this.router.navigateByUrl("/story/999")
    else
      this.router.navigateByUrl("/terms")
  }

  onOverscrollBottomMax(): void {
    this.increaseIndex(true)
  }

  onOverscrollTopMax(): void {
    this.decreaseIndex(true)
  }
}
