import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillsComponent } from './skills/skills.component';
import { NavComponent } from './nav/nav.component';
import { ViewsModule } from '../views/views.module';
import { ContentPageComponent } from './content-page/content-page.component';



@NgModule({
  declarations: [
    SkillsComponent,
    NavComponent,
    ContentPageComponent,
  ],
  imports: [
    CommonModule,
    ViewsModule
  ],
  exports: [
    SkillsComponent
  ]
})
export class SkillsModule { }
