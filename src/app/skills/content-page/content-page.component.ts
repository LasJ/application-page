import { Component, OnInit, Input } from '@angular/core';
import { valueChangeAnimation } from 'src/app/animations';
import { SkillCategory, SkillItem } from 'src/app/data/models';

@Component({
  selector: 'app-content-page',
  templateUrl: './content-page.component.html',
  styleUrls: ['./content-page.component.scss'],
  animations: [valueChangeAnimation]
})
export class ContentPageComponent implements OnInit {

  @Input() skillCategories: SkillCategory[] = []

  @Input() 
  get currentIndex(): [number, number] {
    return this._currentIndex
  }
  set currentIndex(currentIndex: [number, number]) {
    this._currentIndex = currentIndex
    this.updateSkillItem()
  }
  _currentIndex: [number, number] = [0,0]

  currentSkillItem: SkillItem | undefined

  constructor() { }

  ngOnInit(): void { }

  private updateSkillItem(): void {
    let category: SkillCategory | undefined = this.skillCategories[this.currentIndex[0]]
    let items: SkillItem[] | undefined = category?.items
    this.currentSkillItem = items ? items[this.currentIndex[1]] : undefined
  }
}