import { trigger, transition, style, query, group, animateChild, animate } from "@angular/animations";

export const valueChangeAnimation =
  trigger('valueChangeFade', [
    transition(':increment, :decrement', [
      style({ opacity: 0 }),
      animate('500ms', style({ opacity: 1})),
    ])
  ])

export const slideInAnimation =
  trigger('routeAnimations', [
    transition('WelcomePage => StoryPage, WelcomePage => SkillsPage, WelcomePage => TermsPage, WelcomePage => ContactPage, StoryPage => SkillsPage, StoryPage => TermsPage, StoryPage => ContactPage, SkillsPage => TermsPage, SkillsPage => ContactPage, TermsPage => ContactPage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          width: '100%',
          height: '100%'
        })
      ]),
      query(':enter', [
        style({ top: '100%' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('500ms ease-in', style({ top: '-100%' }))
        ]),
        query(':enter', [
          animate('500ms ease-in', style({ top: '0%' }))
        ])
      ]),
      query(':enter', animateChild()),
    ]),
    transition('ContactPage => TermsPage, ContactPage => SkillsPage, ContactPage => StoryPage, ContactPage => WelcomePage, TermsPage => SkillsPage, TermsPage => StoryPage, TermsPage => WelcomePage, SkillsPage => StoryPage, SkillsPage => WelcomePage, StoryPage => WelcomePage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            width: '100%',
            height: '100%'
          })
        ]),
        query(':enter', [
          style({ top: '-100%' })
        ]),
        query(':leave', animateChild()),
        group([
          query(':leave', [
            animate('500ms ease-in', style({ top: '100%' }))
          ]),
          query(':enter', [
            animate('500ms ease-in', style({ top: '0%' }))
          ])
        ]),
        query(':enter', animateChild()),
      ]),
  ]);
