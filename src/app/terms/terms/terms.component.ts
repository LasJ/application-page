import { Component, OnInit } from '@angular/core';
import { ScrollHandler, ScrollListener } from 'src/app/helper/scroll-handler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit, ScrollListener {

  scrollHandler: ScrollHandler = ScrollHandler.getInstance(this)

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    let contentView = document.getElementById("content-terms")
    if (contentView)
      this.scrollHandler.init(contentView)
  }

  ngOnDestroy(): void {
    this.scrollHandler.kill()
  }

  onOverscrollBottomMax(): void {
    this.router.navigateByUrl("/contact")
  }

  onOverscrollTopMax(): void {
    this.router.navigateByUrl("/skills/99/99")
  }
}
