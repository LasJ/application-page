import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsComponent } from './terms/terms.component';
import { ViewsModule } from '../views/views.module';



@NgModule({
  declarations: [
    TermsComponent
  ],
  imports: [
    CommonModule,
    ViewsModule
  ],
  exports: [
    TermsComponent
  ]
})
export class TermsModule { }
