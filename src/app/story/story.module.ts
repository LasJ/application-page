import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewsModule } from '../views/views.module';
import { StoryComponent } from './story/story.component';
import { NavComponent } from './nav/nav.component';
import { CircleComponent } from './nav/circle/circle.component';
import { ContentPageComponent } from './content-page/content-page.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    StoryComponent,
    NavComponent,
    CircleComponent,
    ContentPageComponent
  ],
  imports: [
    CommonModule,
    ViewsModule,
    RouterModule
  ],
  exports: [
    StoryComponent
  ]
})
export class StoryModule { }
