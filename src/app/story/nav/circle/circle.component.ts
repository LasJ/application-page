import { Component, OnInit, Input } from '@angular/core';
import { StoryItem } from 'src/app/data/models';

@Component({
  selector: 'app-circle',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.scss']
})
export class CircleComponent implements OnInit {

  @Input() item!: StoryItem
  @Input() selected: boolean | undefined

  constructor() {
  }

  ngOnInit(): void {
  }

}
