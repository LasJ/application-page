import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StoryItem } from 'src/app/data/models';

const circleHeight: number = 72

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Input() storyItems: StoryItem[] = []
  @Input()
  get currentIndex(): number {return this._currentIndex}
  set currentIndex(currentIndex: number) {
    this._currentIndex = currentIndex
    this.scrollTo(this.currentIndex)
  }
  private _currentIndex: number = 0
  
  @Output() currentIndexChange: EventEmitter<number> = new EventEmitter()
  
  constructor() { }

  ngOnInit(): void { 
  }

  ngAfterViewInit(): void {
    this.setScrollListener()
    this.scrollTo(this.currentIndex)
  }

  setScrollListener() {
    let parentView = document.getElementById("nav-story")
    if (!parentView)
      return
    parentView.addEventListener('wheel', (event) => {
      if (event.deltaY < 0) 
        this.changeCurrentIndex (this.currentIndex -1)
      else if (event.deltaY > 0)
        this.changeCurrentIndex(this.currentIndex + 1)
    })
  }

  public changeCurrentIndex(newIndex: number) {
    if (newIndex >= this.storyItems.length || newIndex < 0)
      return
    this.currentIndexChange.emit(newIndex)
  }

  private scrollTo(index: number) {
    let nav = document.getElementById("nav-story")
    let scrollTarget = index * circleHeight
    nav?.scrollTo(scrollTarget, scrollTarget)
  }
}