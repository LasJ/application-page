import { Component, OnInit } from '@angular/core';
import { StoryItem } from 'src/app/data/models';
import { DataService } from 'src/app/data/data.service';
import { ScrollHandler, ScrollListener } from 'src/app/helper/scroll-handler';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})

export class StoryComponent implements OnInit, ScrollListener {

  contentView: HTMLElement | null = null
  scrollHandler: ScrollHandler = ScrollHandler.getInstance(this)

  storyItems: StoryItem[] = []
  _currentIndex: number = 0
  get currentIndex() {
    return this._currentIndex
  }
  set currentIndex(currentIndex: number) {
    this._currentIndex = currentIndex
    this.updateContent()
  }

  subRouteSubscription: Subscription | undefined

  constructor(private dataService: DataService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getStoryItems()
    this.startSubRouteSubscription()
  }

  ngAfterViewInit(): void {
    this.setScrollHandler()
  }

  ngOnDestroy(): void {
    this.scrollHandler.kill()
    this.subRouteSubscription?.unsubscribe()
  }

  private getStoryItems(): void {
    this.dataService.getStoryItems().subscribe(
      items => {
        this.storyItems = items
        if (this.currentIndex > items.length -1)
          this.currentIndex = items.length -1
      })
  }

  private startSubRouteSubscription(): void {
    this.subRouteSubscription = this.activatedRoute.firstChild?.params.subscribe(params => {
      this.currentIndex = +params['id']
    });
  }

  private setScrollHandler(): void {
    this.contentView = document.getElementById("content-story")
    if (this.contentView)
      this.scrollHandler.init(this.contentView)
  }

  private updateContent() {
    if (this.currentIndex >= this.storyItems.length)
      this.router.navigateByUrl("/story/" + (this.storyItems.length - 1), { replaceUrl: true })
    else if (this.currentIndex < 0 )
      this.router.navigateByUrl("/story/0", { replaceUrl: true })
    else
      this.router.navigateByUrl("/story/" + this.currentIndex, { replaceUrl: true })
    this.resetContentScroll()
  }

  private resetContentScroll() {
    setTimeout(() => { this.contentView?.scrollTo(0, 0) }, 1);
  }

  private leave(prev: boolean) {
    if (prev)
      this.router.navigateByUrl("/")
    else
      this.router.navigateByUrl("/skills/0/0")
  }

  onOverscrollBottomMax(): void {
    if (this.currentIndex >= this.storyItems.length - 1)
      this.leave(false)
    else
      this.currentIndex = this.currentIndex + 1
  }

  onOverscrollTopMax(): void {
    if (this.currentIndex <= 0)
      this.leave(true)
    else
      this.currentIndex = this.currentIndex - 1
  }
}