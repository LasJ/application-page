import { Component, OnInit, Input } from '@angular/core';
import { StoryItem } from 'src/app/data/models';
import { query, transition, trigger, style, animate } from '@angular/animations';
import { valueChangeAnimation } from 'src/app/animations';

@Component({
  selector: 'app-content-page',
  templateUrl: './content-page.component.html',
  styleUrls: ['./content-page.component.scss'],
  animations: [
    valueChangeAnimation
  ]
})
export class ContentPageComponent implements OnInit {

  @Input() storyItems: StoryItem[] = []
  @Input()
  get currentIndex(): number {
    return this._currentIndex
  }
  set currentIndex(currentIndex: number) {
    this._currentIndex = currentIndex
    this.currentStoryItem = this.storyItems[this.currentIndex]
  }

  _currentIndex: number = 0

  public currentStoryItem: StoryItem | undefined = this.storyItems[this.currentIndex]

  constructor() { }

  ngOnInit(): void { }

  ngOnDestroy() { }

}