import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome/welcome.component';
import { StoryComponent } from './story/story/story.component';
import { SkillsComponent } from './skills/skills/skills.component';
import { TermsComponent } from './terms/terms/terms.component';
import { ContactComponent } from './contact/contact/contact.component';
import { ContentPageComponent as ContentPageStory } from './story/content-page/content-page.component';
import { ContentPageComponent as ContentPageSkills } from './skills/content-page/content-page.component';

const routes: Routes = [
  {path: '', component: WelcomeComponent, data: { animation: 'WelcomePage' }},
  {path: 'story', component: StoryComponent, data: { animation: 'StoryPage' },
    children: [
      { path: ':id', component: ContentPageStory }
    ]
  },
  {path: 'skills', component: SkillsComponent, data: { animation: 'SkillsPage' },
    children: [
      { path: ':id1/:id2', component: ContentPageSkills }
    ]
  },
  {path: 'terms', component: TermsComponent, data: { animation: 'TermsPage' }},
  {path: 'contact', component: ContactComponent, data: { animation: 'ContactPage' }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
