import { Component } from '@angular/core';
import { Router, RouterOutlet, Event, NavigationEnd } from '@angular/router';
import { slideInAnimation } from '../animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slideInAnimation
  ]
})
export class AppComponent {

  introActive: Boolean = true

  constructor(private router: Router) {}
  
  title = 'application'
  url = ''

  ngOnInit(): void {
    this.listenForIntro()
  }

  listenForIntro(): void {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        const url = this.router.url
        this.url = url.substring(1).toUpperCase()
        let slashIndex = this.url.indexOf("/")
        if (slashIndex > -1)
          this.url = this.url.substring(0, slashIndex)
        if (url.length > 1) {
          if (this.introActive)
            this.introActive = false
        } else if (!this.introActive && this.router.url.length <= 1)
          this.introActive = true
      }
    })
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet?.activatedRouteData?.['animation'];
  }
}