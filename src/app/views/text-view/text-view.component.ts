import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-view',
  templateUrl: './text-view.component.html',
  styleUrls: ['./text-view.component.scss']
})
export class TextViewComponent implements OnInit {

  @Input() htmlInput: string | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
