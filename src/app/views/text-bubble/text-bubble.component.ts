import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-bubble',
  templateUrl: './text-bubble.component.html',
  styleUrls: ['./text-bubble.component.scss']
})
export class TextBubbleComponent implements OnInit {

  @Input() subtitle: String | undefined
  @Input() bold: boolean | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
