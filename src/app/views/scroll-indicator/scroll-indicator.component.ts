import { Component, OnInit, Input } from '@angular/core';
import { ScrollHandler } from 'src/app/helper/scroll-handler';

@Component({
  selector: 'app-scroll-indicator',
  templateUrl: './scroll-indicator.component.html',
  styleUrls: ['./scroll-indicator.component.scss']
})
export class ScrollIndicatorComponent implements OnInit {

  @Input() scrollHandler!: ScrollHandler | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
