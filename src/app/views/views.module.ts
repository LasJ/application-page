import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextBubbleComponent } from './text-bubble/text-bubble.component';
import { TextViewComponent } from './text-view/text-view.component';
import { ImageGaleryComponent } from './image-galery/image-galery.component';
import { VideoGaleryComponent } from './video-galery/video-galery.component';
import { ScrollIndicatorComponent } from './scroll-indicator/scroll-indicator.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';



@NgModule({
  declarations: [
    TextBubbleComponent,
    TextViewComponent,
    ImageGaleryComponent,
    VideoGaleryComponent,
    ScrollIndicatorComponent,
    ImageSliderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TextBubbleComponent,
    TextViewComponent,
    ImageGaleryComponent,
    ImageSliderComponent,
    VideoGaleryComponent,
    ScrollIndicatorComponent
  ]
})
export class ViewsModule { }
