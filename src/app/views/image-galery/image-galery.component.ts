import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-galery',
  templateUrl: './image-galery.component.html',
  styleUrls: ['./image-galery.component.scss']
})
export class ImageGaleryComponent implements OnInit {

  @Input() imageUrls: String[] | undefined
  @Input() maxHeight: String | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
