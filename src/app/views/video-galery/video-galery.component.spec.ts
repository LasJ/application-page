import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoGaleryComponent } from './video-galery.component';

describe('VideoGaleryComponent', () => {
  let component: VideoGaleryComponent;
  let fixture: ComponentFixture<VideoGaleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoGaleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoGaleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
