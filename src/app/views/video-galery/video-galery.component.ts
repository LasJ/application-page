import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-video-galery',
  templateUrl: './video-galery.component.html',
  styleUrls: ['./video-galery.component.scss']
})
export class VideoGaleryComponent implements OnInit {

  @Input() videoUrls: String[] | undefined
  @Input() maxHeight: String | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
