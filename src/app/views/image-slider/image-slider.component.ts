import { Component, OnInit, Input, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.scss']
})
export class ImageSliderComponent implements OnInit {

  @Input() imageUrls: String[] | undefined
  @Input() maxHeight: String | undefined
  @Input() maxWidth: String | undefined

  constructor() { }

  url: String | undefined
  currentUrlndex = 0;
  rotationInterval: ReturnType<typeof setInterval> | undefined

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.clearRotation()
  }

  ngOnChanges(changes: SimpleChange) {
    this.clearRotation()
    this.resetImage()
    this.setNextImage()
    this.startRotation()
  }

  private startRotation(): void {
    this.rotationInterval = setInterval(this.setNextImage, 2000)
  }

  private clearRotation(): void {
    if(this.rotationInterval != undefined) {
      clearInterval(this.rotationInterval)
    }
  }

  private setNextImage = () => {
    if (this.imageUrls == undefined)
      return
    this.url = this.imageUrls[this.currentUrlndex]
    const nextImage = this.imageUrls.length > this.currentUrlndex + 1
    this.currentUrlndex = nextImage ? this.currentUrlndex +1 : 0
  }

  private resetImage(): void {
    this.url = undefined
    this.currentUrlndex = 0
  }
}
