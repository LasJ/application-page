import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './main/app.component';

import { ContactModule } from './contact/contact.module';
import { MenuModule } from './menu/menu.module';
import { SkillsModule } from './skills/skills.module';
import { StoryModule } from './story/story.module';
import { TermsModule } from './terms/terms.module';
import { WelcomeModule } from './welcome/welcome.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ContactModule,
    MenuModule,
    SkillsModule,
    StoryModule,
    TermsModule,
    WelcomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
