import { Component, OnInit, Input, ViewChild, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() iconPath!: string
  @ViewChild('image') imageView: ElementRef<HTMLDivElement> | undefined 

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.setIcon(this.iconPath)
  }

  private setIcon(url: string) {
    //document.getElementById("image")?.style.setProperty("-webkit-mask", "url("+url+")")
    //document.getElementById("image")?.style.setProperty("-webkit-mask", "url("+url+")")
    let convertedUrl: String = "url(" + url + ")"
    if (this.imageView) {
      this.renderer.setStyle(this.imageView.nativeElement, "-webkit-mask", convertedUrl)
      // This part is necessary since scss (or css) fails to work with the line above
      this.renderer.setStyle(this.imageView.nativeElement, "-webkit-mask-repeat", "no-repeat")
      this.renderer.setStyle(this.imageView.nativeElement, "-webkit-mask-position", "center")
      this.renderer.setStyle(this.imageView.nativeElement, "-webkit-mask-size", "70%")
      this.renderer.setStyle(this.imageView.nativeElement, "mask", convertedUrl)
      this.renderer.setStyle(this.imageView.nativeElement, "mask-repeat", "no-repeat")
      this.renderer.setStyle(this.imageView.nativeElement, "mask-position", "center")
      this.renderer.setStyle(this.imageView.nativeElement, "mask-size", "70%")
      // ..
    }
    
  }
}
