import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ height: 0, opacity: 0 }),
        animate('250ms ease-in',
          style({ height: '*', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ height: '*', opacity: 1 }),
        animate('250ms ease-out',
          style({ height: 0, opacity: 0 }))
      ])
    ])
  ]
})
export class MenuComponent implements OnInit {

  expanded: Boolean = false
  introActive: Boolean = true

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.listenForIntro()
  }

  toggleExpansion(): void {
    this.expanded = !this.expanded
  }

  listenForIntro(): void {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        if (this.introActive && this.router.url.length > 1)
          this.introActive = false
        else if (!this.introActive && this.router.url.length <= 1)
          this.introActive = true
      }
    })
  }
}
