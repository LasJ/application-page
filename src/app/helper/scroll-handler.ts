import * as _ from "lodash";

export interface ScrollListener {
    onOverscrollTopMax(): void
    onOverscrollBottomMax(): void
}

export class ScrollHandler {

    // Construction
    private static instance: ScrollHandler;
    static getInstance(listener: ScrollListener): ScrollHandler {
        if (!ScrollHandler.instance) {
            ScrollHandler.instance = new ScrollHandler(listener);
        } else {
            ScrollHandler.instance.listener = listener
        }
    
        return ScrollHandler.instance;
      }
    private constructor(private listener: ScrollListener) { }

    private scrollView: HTMLElement | undefined
    // ..

    // Public scroll values
    public overscrollTop: number = 0
    public overscrollBottom: number = 0

    private setScrollValues(top: number, bottom: number) {
        this.overscrollTop = top
        this.overscrollBottom = bottom
    }

    private resetScrollValues(): void {
        this.overscrollTop = 0
        this.overscrollBottom = 0
    }
    // ..

    // Initialization
    public init(scrollView: HTMLElement): void {
        if (this.scrollView)
            this.kill()
        this.scrollView = scrollView
        this.scrollView.addEventListener('touchstart', this.touchStartEventListener, true)
        this.scrollView.addEventListener('touchmove', this.touchMoveEventListener, true)
        this.scrollView.addEventListener('touchend', this.touchEndEventListener, true)
        this.scrollView.addEventListener('wheel', this.wheelEventListener, true)
        document.addEventListener('keydown', this.keyEventListener, true)
    }

    public kill() {
        if (!this.scrollView)
            return
        this.scrollView.removeEventListener('touchstart', this.touchStartEventListener, true)
        this.scrollView.removeEventListener('touchmove', this.touchMoveEventListener, true)
        this.scrollView.removeEventListener('touchend', this.touchEndEventListener, true)
        this.scrollView.removeEventListener('wheel', this.wheelEventListener, true)
        document.removeEventListener('keydown', this.keyEventListener, true)
        this.scrollView = undefined
    }
    // ..

    // Swipe handling
    private swipeThreshold: number = 150
    private yStart: number | null = null
    private yToTopStart: number | null = null
    private yToBottomStart: number | null = null
    private yOverswipeTop: number | null = null
    private yOverswipeBottom: number | null = null

    private touchStartEventListener = (event: TouchEvent) => {this.handleTouchStart(event)}

    handleTouchStart(evt: TouchEvent) {
        if (!this.scrollView)
            return
        this.resetSwipeValues()
        this.yStart = this.getCurrentTouch(evt).clientY
        this.yToTopStart = this.scrollView.scrollTop
        this.yToBottomStart = this.scrollView.scrollHeight - (this.scrollView.clientHeight + this.scrollView.scrollTop)
        this.setScrollValues(0,0)
    }

    private touchMoveEventListener = _.throttle((event: TouchEvent) => {this.handleTouchMove(event)}, 50)

    handleTouchMove(evt: TouchEvent) {
        if (this.yStart == null || !this.scrollView || this.yToBottomStart == null || this.yToTopStart == null)
            return
        const touch = this.getCurrentTouch(evt)
        let yEnd = touch.clientY

        let yOverswipeBottomTemp = 0
        yOverswipeBottomTemp += this.yStart - yEnd - this.yToBottomStart
        this.yOverswipeBottom = yOverswipeBottomTemp > 0 ? yOverswipeBottomTemp : 0

        let overswipeTopTemp = 0
        overswipeTopTemp += yEnd - this.yStart - this.yToTopStart
        this.yOverswipeTop = overswipeTopTemp > 0 ? overswipeTopTemp : 0

        this.setScrollValues(this.yOverswipeTop / this.swipeThreshold, this.yOverswipeBottom / this.swipeThreshold)
    }

    private touchEndEventListener = () => {this.handleTouchEnd()}

    handleTouchEnd() {
        if (!this.scrollView)
            return;
        if (this.yOverswipeTop != null && this.yOverswipeTop >= this.swipeThreshold)
            this.listener.onOverscrollTopMax()
        else if (this.yOverswipeBottom != null && this.yOverswipeBottom >= this.swipeThreshold)
            this.listener.onOverscrollBottomMax()

        this.resetSwipeValues()
        this.resetScrollValues()
    }

    getCurrentTouch(evt: TouchEvent): Touch {
        return evt.touches[0]
    }

    resetSwipeValues(): void {
        this.yStart = null
        this.yToTopStart = null
        this.yToBottomStart = null
        this.yOverswipeTop = null
        this.yOverswipeBottom = null
    }
    // ..

    // Scroll handling
    private wheelThreshold = 4
    private wheelTimeout: ReturnType<typeof setTimeout> | undefined = undefined
    public wheelCountTop: number = 0
    public wheelCountBottom: number = 0

    private touchpadBlock: boolean = false;
    private touchpadBlockTimeout: ReturnType<typeof setTimeout> | undefined = undefined
    private oldDeltaY: number = 0;

    private wheelEventListener = _.throttle((event: WheelEvent) => {this.handleWheel(event)},20)

    handleWheel(event: WheelEvent): void {
        if (!this.scrollView)
            return
        if (Math.abs(event.deltaY) < 5)
            return
        if (this.touchpadBlock && Math.abs(event.deltaY) <= this.oldDeltaY) {
            this.resetTouchpadBlock()
            return
        }
        this.oldDeltaY = Math.abs(event.deltaY)
        let up: Boolean = event.deltaY <= 0

        let scrollLimitReached = this.checkWheelScrollLimit(this.scrollView, up)
        if (!scrollLimitReached)
            return

        this.wheelCountTop = up ? this.wheelCountTop + 1 : 0
        this.wheelCountBottom = up ? 0 : this.wheelCountBottom + 1
        this.setScrollValues(this.wheelCountTop / this.wheelThreshold, this.wheelCountBottom / this.wheelThreshold)

        if (this.wheelCountBottom > this.wheelThreshold) {
            this.listener.onOverscrollBottomMax()
            this.resetScrollValues()
            this.resetTouchpadBlock()
            this.wheelCountTop = -1
            this.wheelCountBottom = -this.wheelThreshold
        } else if (this.wheelCountTop > this.wheelThreshold) {
            this.listener.onOverscrollTopMax()
            this.resetScrollValues()
            this.resetTouchpadBlock()
            this.wheelCountTop = -this.wheelThreshold
            this.wheelCountBottom = -1
        } else {
            this.startWheelResetTimer()
        }
    }

    private checkWheelScrollLimit(view: HTMLElement, up: Boolean): Boolean {
        let remainingDistance: Number = up ? view.scrollTop : view.scrollHeight - (view.clientHeight + view.scrollTop)
        return remainingDistance < 1
    }

    private startWheelResetTimer() {
        this.clearWheelResetTimer()
        this.wheelTimeout = setTimeout(() => this.resetWheelValues(), 200)
    }

    private clearWheelResetTimer() {
        if (this.wheelTimeout)
            clearTimeout(this.wheelTimeout);
    }

    private resetTouchpadBlock() {
        if (this.touchpadBlockTimeout)
            clearTimeout(this.touchpadBlockTimeout)
        this.touchpadBlock = true
        this.touchpadBlockTimeout = setTimeout(() => this.touchpadBlock = false, 50)
    }

    resetWheelValues(): void {
        this.wheelCountTop = 0
        this.wheelCountBottom = 0
        this.resetScrollValues()
    }
    // ..

    // Key handling
    private keyEventListener = (event: KeyboardEvent) => {this.handleKey(event)}
    
    private handleKey(e: KeyboardEvent) {
        e = e || window.event;
        switch (e.key) {
            case "ArrowLeft":
                this.listener.onOverscrollTopMax()
                break;
            case "ArrowRight":
                this.listener.onOverscrollBottomMax()
                break;
            case "ArrowUp":
                this.listener.onOverscrollTopMax()
                break;
            case "ArrowDown":
                this.listener.onOverscrollBottomMax()
                break;
        }
    }
    // ..
}