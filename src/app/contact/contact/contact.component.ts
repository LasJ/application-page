import { Component, OnInit } from '@angular/core';
import { ScrollHandler, ScrollListener } from 'src/app/helper/scroll-handler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, ScrollListener {

  scrollHandler: ScrollHandler = ScrollHandler.getInstance(this)
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    let contentView = document.getElementById("content-contact")
    if (contentView)
      this.scrollHandler.init(contentView)
  }

  ngOnDestroy(): void {
    this.scrollHandler.kill()
  }

  onOverscrollTopMax(): void {
    this.router.navigateByUrl("/terms")
  }

  onOverscrollBottomMax(): void {
    // Do nothing
  }
}
