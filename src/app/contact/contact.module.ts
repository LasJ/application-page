import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact/contact.component';
import { ViewsModule } from '../views/views.module';


@NgModule({
  declarations: [
    ContactComponent
  ],
  imports: [
    CommonModule,
    ViewsModule
  ],
  exports: [
    ContactComponent
  ]
})
export class ContactModule { }
